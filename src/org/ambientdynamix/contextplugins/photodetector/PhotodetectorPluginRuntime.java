/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.photodetector;

import java.util.Date;
import java.util.UUID;

import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.contextplugin.AutoReactiveContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.ContextPluginSettings;
import org.ambientdynamix.api.contextplugin.PowerScheme;
import org.ambientdynamix.api.contextplugin.security.PrivacyRiskLevel;
import org.ambientdynamix.api.contextplugin.security.SecuredContextInfo;
import org.ambientdynamix.api.contextplugin.security.SecuredSensorManager;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;

public class PhotodetectorPluginRuntime extends AutoReactiveContextPluginRuntime implements SensorEventListener {
	private final String TAG = this.getClass().getSimpleName();
	private PowerScheme powerScheme;
	private SecuredSensorManager sensorManager;
	private Sensor lightSensor;
	private double lastEventNormalizedLux = 0;
	private float latestLux = 0;
	private float lastEventLux = 0;
	private boolean lastEventSent;
	private Date lastEventTime;

	public void setPowerScheme(PowerScheme scheme) {
	}

	@Override
	public void init(PowerScheme scheme, ContextPluginSettings settings) throws Exception {
		this.powerScheme = scheme;
		Context c = getSecuredContext();
		sensorManager = (SecuredSensorManager) c.getSystemService(Context.SENSOR_SERVICE);
		if (sensorManager == null)
			throw new Exception("Could not access sensor manager");
		lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
		if (lightSensor == null)
			throw new Exception("Could not access light sensor");
		Log.d(TAG, "Initialized!");
	}

	@Override
	public void start() {
		latestLux = 0;
		lastEventNormalizedLux = 0;
		sensorManager.registerSensorListener(this, lightSensor, SensorManager.SENSOR_DELAY_UI);
		Log.d(TAG, "Started!");
	}

	@Override
	public void stop() {
		sensorManager.unregisterListener(this);
		Log.d(TAG, "Stopped!");
	}

	@Override
	public void destroy() {
		stop();
		sensorManager = null;
		lightSensor = null;
		Log.d(TAG, "Destroyed!");
	}

	@Override
	public void doManualContextScan() {
		// TODO Auto-generated method stub
	}

	@Override
	public void updateSettings(ContextPluginSettings settings) {
		// TODO Auto-generated method stub
	}

	@Override
	public void handleConfiguredContextRequest(UUID requestId, String contextInfoType, Bundle scanConfig) {
		Log.w(TAG, "Scan configuration not supported by: " + this);
		handleContextRequest(requestId, contextInfoType);
	}

	@Override
	public void handleContextRequest(UUID requestId, String contextInfoType) {
		if (contextInfoType.equalsIgnoreCase("org.ambientdynamix.contextplugins.photodetector.luxvalue"))
			sendContextEvent(requestId, new SecuredContextInfo(new PhotodetectorContextInfo(latestLux),
					PrivacyRiskLevel.LOW), 5000);
		else
			sendContextRequestError(requestId, "Context type not supported: " + contextInfoType,
					ErrorCodes.CONTEXT_TYPE_NOT_SUPPORTED);
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
	}

	

	@Override
	public synchronized void onSensorChanged(SensorEvent event) {
		if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
			double normalized;
			boolean sendTimingOk = true;
		
			// Only send events once per second
			if (lastEventTime != null && (new Date().getTime() - lastEventTime.getTime() < 1000)) {
				sendTimingOk = false;
			}
			// Store the lux value as the latestLux
			latestLux = event.values[0];
			// Check for zero-lux case
			if (latestLux == 0) {
				normalized = 0;
			} else {
				// Calculate the normalized lux level (0-1)
				// http://msdn.microsoft.com/en-us/library/windows/desktop/dd319008%28v=vs.85%29.aspx
				normalized = Math.log10(latestLux) / 5;
			}
			boolean sendEvent = false;
			if (lastEventNormalizedLux == 0 && normalized > 0) {
				sendEvent = true;
			} else {
				if (normalized > lastEventNormalizedLux) {
					// Check for increase
					if ((((normalized - lastEventNormalizedLux) / lastEventNormalizedLux) * 100 > 10)) {
						sendEvent = true;
					}
				} else {
					// Check for decrease
					if ((((lastEventNormalizedLux - normalized) / lastEventNormalizedLux) * 100 > 10)) {
						sendEvent = true;
					}
				}
			}
			if(sendEvent){
				if(sendTimingOk)
					processEvent(normalized, latestLux, true);
				else
					processEvent(normalized, latestLux, false);
			}
			// Check for proper event timing
			if (sendTimingOk && sendEvent)
				processEvent(normalized, latestLux, true);
			else if(!sendTimingOk && sendEvent)
				processEvent(normalized, latestLux, false);
			else if (sendTimingOk && !sendEvent && !lastEventSent){
				//Log.d(TAG, "Processing cached lux event: " + lastEventLux);
				processEvent(normalized, lastEventLux, true);
			}
			
		}
	}

	private void processEvent(double normalizedLux, float lux, boolean send) {
		// Log.d(TAG, "Send event: " + latestLux);
		lastEventNormalizedLux = normalizedLux;
		lastEventLux = lux;
		if(send){
			sendBroadcastContextEvent(new SecuredContextInfo(new PhotodetectorContextInfo(lux), PrivacyRiskLevel.LOW), 5000);
			// Log event time and status
			lastEventTime = new Date();
			lastEventSent = true;	
		}
		else {
			lastEventSent = false;	
			//Log.d(TAG, "Delaying lux event: " + lux);
		}
	}
}