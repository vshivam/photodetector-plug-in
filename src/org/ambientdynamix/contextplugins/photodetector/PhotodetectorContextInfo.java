/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.photodetector;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import org.ambientdynamix.api.application.IContextInfo;

import android.location.Location;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;

class PhotodetectorContextInfo implements IPhotodetectorContextInfo {
	/**
	 * Required CREATOR field that generates instances of this Parcelable class from a Parcel.
	 * 
	 * @see http://developer.android.com/reference/android/os/Parcelable.Creator.html
	 */
	public static final Parcelable.Creator<PhotodetectorContextInfo> CREATOR = new Parcelable.Creator<PhotodetectorContextInfo>() {
		public PhotodetectorContextInfo createFromParcel(Parcel in) {
			return new PhotodetectorContextInfo(in);
		}

		public PhotodetectorContextInfo[] newArray(int size) {
			return new PhotodetectorContextInfo[size];
		}
	};
	private float luxValue;

	public PhotodetectorContextInfo(float luxValue) {
		this.luxValue = luxValue;
	}

	@Override
	public float getLuxValue() {
		return luxValue;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<String> getStringRepresentationFormats() {
		Set<String> formats = new HashSet<String>();
		formats.add("text/plain");
		return formats;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getStringRepresentation(String format) {
		if (format.equalsIgnoreCase("text/plain"))
			return String.valueOf(luxValue);
		else
			return "";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getImplementingClassname() {
		return this.getClass().getName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getContextType() {
		return "org.ambientdynamix.contextplugins.photodetector.luxvalue";
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	};

	public IBinder asBinder() {
		return null;
	}

	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel out, int arg1) {
		out.writeFloat(luxValue);
	}

	private PhotodetectorContextInfo(final Parcel in) {
		luxValue = in.readFloat();
	}
}
